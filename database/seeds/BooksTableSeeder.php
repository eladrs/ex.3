<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'title' => 'Born to Run',
                'author' => 'Christopher McDougall ',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Predictably Irrational, Revised and Expanded Edition',
                'author' => 'Dan Ariely  ',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Rich Dad Poor Dad',
                'author' => 'Robert T. Kiyosaki ',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Sapiens: A Brief History of Humankind',
                'author' => 'Yuval Noah Harari ',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'The Alchemist',
                'author' => 'Paulo Coelho ',
                'created_at' => date('Y-m-d G:i:s')
            ]

        ]);
    }
}
